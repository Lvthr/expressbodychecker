const ERRORS = require('./errors.js');
const BASIC_CHECKS = require('./basic_checks.js');
const CHECKTYPES = require('./checktypes.js');
const BAD_REQUEST = 400;
const DEFAULT_FAILUREFUNCTION = (req, res, errors) => {
    res.status(BAD_REQUEST);
    res.json(errors);
};

function get_checkdetails(check_details) {
    let output = {};
    if(check_details !== undefined) {
        if(check_details.checktype !== undefined) {
            output.checktype = check_details.checktype;
        } else {
            output.checktype = CHECKTYPES.DEFAULT;
        }

        if(check_details.failurefunction !== undefined) {
            output.failurefunction = check_details.failurefunction;
        } else {
            output.failurefunction = DEFAULT_FAILUREFUNCTION;
        }
    } else {
        output.checktype = CHECKTYPES.DEFAULT;
        output.failurefunction = DEFAULT_FAILUREFUNCTION;
    }
    return output;
}


function check_body(bodytype, check_details) {
    let checkdetails = get_checkdetails(check_details);
    let failurefunction = checkdetails.failurefunction;
    let checktype = checkdetails.checktype;

    return function(req, res, next) {
        
        let request_info = {
            req: req,
            res: res,
            next: next,
            bodytype: bodytype,
            failurefunction: failurefunction,
            checktype: checktype
        };

        /*
        bodytype examples:
        {
            name: String, age: Number, ...
        }
        {
            name: {
                type: String,
                min/max...
            }
        }
        {
            name: String,
            job: {
                description: String,
                salary: Number
            }
        }
        */ 
        recursive(request_info, bodytype, req.body, (valid, errors) => {
            if(!valid) {
                failurefunction(req, res, errors);
            } else {
                next();
            }
        });
    }
}

function recursive(request_info, definition, value, callback) { // definition is allways an object
    /*
    definition examples:
    {
            name: String, age: Number, ...
    }
    {
        name: {
            type: String,
            min/max...
        }
    }
    {
        name: String,
        job: {
            description: String,
            salary: Number
        }
    }
    */
    let errors = [];
    for(let key in definition) {
        let typedef = type(definition[key]);
        if(value.hasOwnProperty(key)) {
            if(typedef === 'Object') {
                let helper = request_info.parent_key;
                request_info.parent_key = key;
                recursive(request_info, definition[key], value[key], (valid, error_list) => {
                    if(!valid) {
                        for(let i = 0; i < error_list.length; i++) {
                            errors.push(error_list[i]);
                        }
                    }
                });
                request_info.parent_key = helper;
                /*
                if(definition[key].hasOwnProperty('type')) {
                    /*
                    definition: {
                        name: {
                            type: String,
                            minlength/maxlength...
                        }
                    }
                    value: {
                        name: 'John'
                    }
                    /
                    detail(request_info, definition, value, (valid, error_list) => {
                        if(!valid) {
                            for(let i = 0; i < error_list.length; i++) {
                                errors.push(error_list[i]);
                            }
                        }
                    });
                } else {
                    /*
                    definition: {
                        job: {
                            description: String,
                            ...
                        }
                    }
                    value: {
                        job: {
                            description: 'MyJob'
                        }
                    }
                    /
                    
                }
                */
            } else if(typedef === 'Array') {
                if(Array.isArray(value[key])) {
                    if(definition[key].hasOwnProperty('type')) {
                        BASIC_CHECKS.arraychecker(value[key], key, definition[key], (valid, error) => {
                            if(!valid) {
                                errors.push(error);
                            }
                        });
                        if(definition[key].hasOwnProperty('element_type')) {
                            let helper = request_info.parent_key;
                            request_info.parent_key = key;
                            itterate_array(request_info, definition[key].element_type, value[key], (valid, error_list) => {
                                if(!valid) {
                                    for(let i = 0; i < error_list.length; i++) {
                                        errors.push(error_list[i]);
                                    }
                                }
                                request_info.parent_key = helper;
                            });
                        }
                    }
                } else {
                    errors.push(ERRORS.functions.INCORRECT_TYPE(key, typedef, typeof value));
                }
            } else if(typedef === 'String') {
                if(typeof value[key] !== 'string') {
                    errors.push(ERRORS.functions.INCORRECT_TYPE(key, typedef, typeof value[key]));
                } else {
                    if(definition[key].hasOwnProperty('type')) {
                        BASIC_CHECKS.stringchecker(value[key], key, definition[key], (valid, error) => {
                            if(!valid) {
                                errors.push(error);
                            }
                        });
                    }
                }
            } else if(typedef === 'Number') {
                if(typeof value[key] !== 'number') {
                    errors.push(ERRORS.functions.INCORRECT_TYPE(key, typedef, typeof value[key]));
                } else {
                    if(definition[key].hasOwnProperty('type')) {
                        BASIC_CHECKS.numberchecker(value[key], key, definition[key], (valid, error) => {
                            if(!valid) {
                                errors.push(error);
                            }
                        });
                    }
                }
            } else if(typedef === 'Boolean') {
                if(typeof value[key] !== 'boolean') {
                    errors.push(ERRORS.functions.INCORRECT_TYPE(key, typedef, typeof value[key]));
                } else {
                    // Do nothing, no basic check for booleans, does not make sence to have it
                }
            }
        } else {
            errors.push(ERRORS.functions.MISSING_KEY(key, typedef));
        }
    }
    
    if(request_info.checktype === CHECKTYPES.EXPLICIT) {
        for(let key in value) {
            if(!definition.hasOwnProperty(key)) {
                // The body contains a key that is not allowed
                errors.push(ERRORS.functions.EXCESSIVE_KEY(key, typeof value[key]));
            }
        }
    }

    if(errors.length > 0) {
        callback(false, errors);
    } else {
        callback(true, null);
    }
}

/*
function detail(request_info, definition, value, callback) {
    console.log(definition, value)
    /*
    definition: {
        type: String,
        minlengt/max...
    }
    value: 'String'/42/true
    /

    let typedef = type(definition);
    if(typedef === 'String') {
        if(typeof value !== 'string') {
            callback(false, ERRORS.functions.INCORRECT_TYPE(request.parent_key, typedef, typeof value));
        }
    } else if(typedef === 'Number') {
        if(typeof value !== 'number') {
            callback(false, ERRORS.functions.INCORRECT_TYPE(request.parent_key, typedef, typeof value));
        }
    } else if(typedef === 'Boolean') {
        if(typeof value !== 'boolean') {
            callback(false, ERRORS.functions.INCORRECT_TYPE(request.parent_key, typedef, typeof value));
        }
    } 
    callback(true, null);
}
*/

function itterate_array(request_info, element_definition, array, callback) {
    /*
    element_definition: {
        name: {
            type: String,
            ...
        },
        job: {
            description: {
                type: String,
                ...
            },
            salary: Number
        },
        ...
    }
    array: [
        {...}
    ]
    */
    //console.log(element_definition, array)
    let errors = [];
    for(let i = 0; i < array.length; i++) {
        let element = array[i];
        //console.log(element, element_definition)
        let typedef = type(element_definition);
        if(typedef === 'Object') {
            recursive(request_info, element_definition, element, (valid, error_list) => {
                if(!valid) {
                    for(let j = 0; j < error_list.length; j++) {
                        let err = error_list[j];
                        if(err.code === ERRORS.codes.INCORRECT_TYPE_CODE) {
                            errors.push(ERRORS.functions.ARRAY_ELEMENT_INCORRECT_TYPE(i, err.key, err.expected_type, err.found_type));
                        } else if(err.code === ERRORS.codes.MISSING_KEY_CODE) {
                            errors.push(ERRORS.functions.ARRAY_ELEMENT_MISSING_KEY(i, err.key, err.expected_type))
                        } else if(err.code === ERRORS.codes.EXCESSIVE_KEY_CODE) {
                            errors.push(ERRORS.functions.ARRAY_ELEMENT_EXCESSIVE_KEY(i, err.key, err.found_type));
                        }
                    }
                }
            });
        } else if(typedef === 'String') {
            if(typeof element !== 'string') {
                errors.push(ERRORS.functions.ARRAY_ELEMENT_INCORRECT_TYPE(i, request_info.parent_key, typedef, typeof element));
            } else {
                if(element_definition.hasOwnProperty('type')) {
                    BASIC_CHECKS.stringchecker(element, 'Array (' + request_info.parent_key + ') element ' + i, element_definition, (valid, error) => {
                        if(!valid) {
                            errors.push(error);
                        }
                    });
                }
            }
        } else if(typedef === 'Number') {
            if(typeof element !== 'number') {
                errors.push(ERRORS.functions.ARRAY_ELEMENT_INCORRECT_TYPE(i, request_info.parent_key, typedef, typeof element));
            } else {
                if(element_definition.hasOwnProperty('type')) {
                    BASIC_CHECKS.numberchecker(element, 'Array (' + request_info.parent_key + ') element ' + i, element_definition, (valid, error) => {
                        if(!valid) {
                            errors.push(error);
                        }
                    });
                }
            }
        } else if(typedef === 'Boolean') {
            if(typeof element !== 'boolean') {
                errors.push(ERRORS.functions.ARRAY_ELEMENT_INCORRECT_TYPE(i, request_info.parent_key, typedef, typeof element));
            }
        } else if(typedef === 'Array') {
            if(!Array.isArray(element)) {
                errors.push(ERRORS.functions.ARRAY_ELEMENT_INCORRECT_TYPE(i, request_info.parent_key, typedef, typeof element));
            } else {
                recursive(request_info, element_definition, element, (valid, error_list) => {
                    if(!valid) {
                        for(let j = 0; j < error_list.length; j++) {
                            let err = error_list[j];
                            if(err.code === ERRORS.codes.INCORRECT_TYPE_CODE) {
                                errors.push(ERRORS.functions.ARRAY_ELEMENT_INCORRECT_TYPE(i, err.key, err.expected_type, err.found_type));
                            } else if(err.code === ERRORS.codes.MISSING_KEY_CODE) {
                                errors.push(ERRORS.functions.ARRAY_ELEMENT_MISSING_KEY(i, err.key, err.expected_type))
                            } else if(err.code === ERRORS.codes.EXCESSIVE_KEY_CODE) {
                                errors.push(ERRORS.functions.ARRAY_ELEMENT_EXCESSIVE_KEY(i, err.key, err.found_type));
                            }
                        }
                    }
                });
            }
        }
    }
    if(errors.length > 0) {
        callback(false, errors);
    } else {
        callback(true, null);
    }
}

function type(definition_object) {
    if(definition_object.toString() === 'function String() { [native code] }') {
        return 'String'
    } else if(definition_object.toString() === 'function Number() { [native code] }') {
        return 'Number'
    } else if(definition_object.toString() === 'function Boolean() { [native code] }') {
        return 'Boolean'
    } else if(definition_object.toString() === 'function Array() { [native code] }') {
        return 'Array'
    } else if(typeof definition_object === 'object') {
        if(definition_object.hasOwnProperty('type')) {
            let t = type(definition_object.type)
            return t
        } else {
            return 'Object'
        }
    } else {
        return null
    }
}

module.exports = {
    check_body: check_body,
    EXPLICIT: CHECKTYPES.EXPLICIT,
    REQUIRED: CHECKTYPES.REQUIRED
}