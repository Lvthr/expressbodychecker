const bodychecker = require('./bodychecker.js');

let bodytype = {
    list: {
        type: Array,
        element_type: {
            type: Number
        }
    },
    str: {
        type: String
    }
};

let mock_req = {
    body: {
        list: [
            1, 2, 3, 4, 5.6, 7.8, 10.1000001
        ],
        str: 'String',
        key_that_should_fail: 'shouldfail'
    }
};

let mock_res = {
    status: function(statuscode) {},
    json: function(json) {}
};

let checkdetails = {
    failurefunction: function(req, res, errors) {
        console.error(errors);
    },
    checktype: bodychecker.EXPLICIT
};

bodychecker.check_body(bodytype, checkdetails)(mock_req, mock_res, (req, res) => {
    console.log('valid body')
});