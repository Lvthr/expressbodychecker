var assert = require('assert');

const bodychecker = require('../bodychecker.js');
describe('Basic datatypes check V1.6.0', function() {
    describe('Succeeds when the body member is of (expected) basic datatype (String, Number, Boolean)', function() {
            it('Succeeds when member is a String', function(done) {
                let bodytype = {
                    value: String // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
                };

                let mock_req = { body: {value: 'A_String'} };
                let mock_res = {
                    status: function(status_code){},
                    json: function(errors) {
                        done(errors);
                    }
                };

                let checkdetails = {
                    checktype: bodychecker.REQUIRED
                };
                bodychecker.check_body(bodytype, checkdetails)(mock_req, mock_res, () => {
                    done();
                });
            });

            it('Succeeds when member is a Number', function(done) {
                let bodytype = {
                    value: Number, // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
                    another_value: Number
                };
                let mock_req = { body: {value: 42, another_value: 3.1415} };

                let mock_res = {
                    status: function(status_code){},
                    json: function(errors) {
                        done(errors);
                    }
                };
                let checkdetails = {
                    checktype: bodychecker.EXPLICIT
                };
                bodychecker.check_body(bodytype, checkdetails)(mock_req, mock_res, () => {
                    done();
                });
            });

            it('Succeeds when member is a Boolean', function(done) {
                let bodytype = {
                    value: Boolean,
                    another_value: Boolean
                };
                let mock_req = { body: {value: true, another_value: false} };

                let mock_res = {
                    status: function(status_code){},
                    json: function(errors) {
                        //done(errors);
                    }
                };
                let checkdetails = {
                    failurefunction: function(req, res, errors) {
                        done(errors);
                    }
                };
                bodychecker.check_body(bodytype, checkdetails)(mock_req, mock_res, () => {
                    done();
                });
            });
    });

    describe('Fails when the body member is expected to be of basic datatype (String, Number, Boolean), but a different basic datatype was passed', function() {
        it('Fails when member is a String, but receives Number', function(done) {
            let bodytype = {
                value: String // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
            };
            let mock_req = { body: {value: 42} };

            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when member is a String, but receives Boolean', function(done) {
            let bodytype = {
                value: String // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
            };
            let mock_req = { body: {value: true} };

            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when member is a Number, but receives a String', function(done) {
            let bodytype = {
                value: Number, // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
            };
            let mock_req = { body: {value: 'A_String'} };

            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when member is a Number, but receives a Boolean', function(done) {
            let bodytype = {
                value: Number, // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
            };
            let mock_req = { body: {value: true} };

            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when member is a Boolean, but receives a String', function(done) {
            let bodytype = {
                value: Boolean, // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
            };
            let mock_req = { body: {value: "A_String"} };

            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when member is a Boolean, but receives a Number', function(done) {
            let bodytype = {
                value: Boolean, // This is a "basic" datatype, in contrast to a typedef: { value: {type: String, minlength... } }
            };
            let mock_req = { body: {value: 42.5} };

            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });
    });
});


