var assert = require('assert');

const bodychecker = require('../bodychecker.js');
const randombody = require('./random.js');

function get_mock_req(body) {
    let moch_req = {
        body: body
    };
    return moch_req;
}

function get_mock_res() {
    let moch_res = {
        status: function(statuscode) {},
        json: function(data) {}
    };
    return moch_res;
}

function get_checkdetails(shouldfail, done) {
    if(shouldfail === true) {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(); // the request should fail, call done with no errors
            }
        };
        return checkdetails;
    } else {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(errors); // the request should not fail, call done with errors
            }
        };
        return checkdetails;
    }
}

function check(bodytype, body, shouldfail, done) {
    let moch_req = get_mock_req(body);
    let moch_res = get_mock_res();
    let checkdetails = get_checkdetails(shouldfail, done);   

    if(shouldfail === true) {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done('Error: test succeeded but should have failed'); // Expect the request to succeed
        });
    } else {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done(); // Expect the request to succeed
        });
    }
}

const NUMBER_OF_RANDOM_TESTS = 50;
describe('Random body tests', function() {
    for(let i = 0; i < NUMBER_OF_RANDOM_TESTS; i++) {
        it('Should succeed', function(done) {
            let bodytype_and_body = randombody();
            //console.error(bodytype_and_body);
            let shouldfail = false;
    
            check(bodytype_and_body.bodytype, bodytype_and_body.body, shouldfail, done);
        });
    }
});