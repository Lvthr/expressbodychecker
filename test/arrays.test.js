var assert = require('assert');

const bodychecker = require('../bodychecker.js');
function get_mock_req(body) {
    let moch_req = {
        body: body
    };
    return moch_req;
}

function get_mock_res() {
    let moch_res = {
        status: function(statuscode) {},
        json: function(data) {}
    };
    return moch_res;
}

function get_checkdetails(shouldfail, done) {
    if(shouldfail === true) {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(); // the request should fail, call done with no errors
            }
        };
        return checkdetails;
    } else {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(errors); // the request should not fail, call done with errors
            }
        };
        return checkdetails;
    }
}

function check(bodytype, body, shouldfail, done) {
    let moch_req = get_mock_req(body);
    let moch_res = get_mock_res();
    let checkdetails = get_checkdetails(shouldfail, done);   

    if(shouldfail === true) {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done('Error: test succeeded but should have failed'); // Expect the request to succeed
        });
    } else {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done(); // Expect the request to succeed
        });
    }
}
describe('Array tests v1.6.0', function() {
    describe('Succeeds when the body member is of (expected) type Array, with only basic datatype elements.', function() {
        it('Succeeds when array elements are Strings', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: String
                }
            };
            let mock_req = {
                body: {
                    list: [
                        "FirstString", "SecondString", "ThirdString"
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done(errors);
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done();
            });
        });

        it('Succeeds when array elements are Numbers', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Number
                }
            };
            let mock_req = {
                body: {
                    list: [
                        1, 2, 3, 4, 5
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done(errors);
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done();
            });
        });

        it('Succeeds when array elements are Booleans', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Boolean
                }
            };
            let mock_req = {
                body: {
                    list: [
                        true, false, true, true, true
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done(errors);
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done();
            });
        });

        it('Fails when array elements are expected to be Strings, but receives Numbers', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: String
                }
            };
            let mock_req = {
                body: {
                    list: [
                        1, 2, 3, 4, 5.6, 7.8, 10.1000001
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });


        it('Fails when array elements are expected to be Strings, but receives Booleans', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: String
                }
            };
            let mock_req = {
                body: {
                    list: [
                        true, false, true, true, false
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when array elements are expected to be Numbers, but receives Strings', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Number
                }
            };
            let mock_req = {
                body: {
                    list: [
                        'FirstString', 'SecondString', 'ThirdString'
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when array elements are expected to be Numbers, but receives Booleans', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Number
                }
            };
            let mock_req = {
                body: {
                    list: [
                        true, false, true, true, false
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when array elements are expected to be Booleans, but receives Strings', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Number
                }
            };
            let mock_req = {
                body: {
                    list: [
                        'FirstString', 'SecondString', 'ThirdString'
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });

        it('Fails when array elements are expected to be Booleans, but receives Numbers', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Boolean
                }
            };
            let mock_req = {
                body: {
                    list: [
                        1, 2, 3, 4, 5.6, 7.8, 10.1000001
                    ]
                }
            };
            let mock_res = {
                status: function(status_code){},
                json: function(errors) {
                    done();
                }
            };
            bodychecker.check_body(bodytype)(mock_req, mock_res, () => {
                done('Error: the valid_body method succeeded but should have failed');
            });
        });
    });

    describe('Fails when the body member is of (expected) type Array, with only basic datatype elements, but some of the array elements are not of the expected type.', function () {
        it('Fails when array elements are Strings, but one element is a Boolean', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: String
                }
            };
            let body = {
                list: ['String1', 'String2', true]
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when array elements are Numbers, but one element is a string', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Number
                }
            };
            let body = {
                list: ['String1', 1, 2.5]
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when array elements are Booleans, but one element is a Number', function(done) {
            let bodytype = {
                list: {
                    type: Array,
                    element_type: Boolean
                }
            };
            let body = {
                list: [true, false, 2.5]
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    });

    describe('Fails when the body member is expected to be an array of some sort, but the found bodymember is not array', function() {
        it('Fails when the expeced Array is a String', function(done) {
            let bodytype = {
                arr: {
                    type: Array
                }
            };
            let body = {
                arr: 'A String, should fail'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the expeced Array is a Number', function(done) {
            let bodytype = {
                arr: {
                    type: Array
                }
            };
            let body = {
                arr: 666
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the expeced Array is a Boolean', function(done) {
            let bodytype = {
                arr: {
                    type: Array
                }
            };
            let body = {
                arr: false
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
        
        it('Fails when the expeced Array is an Object', function(done) {
            let bodytype = {
                arr: {
                    type: Array
                }
            };
            let body = {
                arr: {
                    str: 'The arr member is an object, should fail',
                    num: 666,
                    bool: false,
                    internal_obj: {
                        str: 'The arr member is an object, should fail',
                        num: 666,
                        bool: false,
                    }
                }
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    });

    describe('The element_type is described to be an object', function() {
        it('Succeeds when the element_type is as expected', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    element_type: {
                        str: String,
                        num: Number,
                        bool: Boolean,
                        internal_obj: {
                            int_str: String,
                            int_num: Number,
                            int_bool: Boolean
                        }
                    }
                }
            };
            let body = {
                arr: [
                    {
                        str: 'Elem1',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false
                        }
                    },
                    {
                        str: 'Elem2',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false
                        }
                    }
                ]
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when one key in on of the body array elements is not as expected', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    element_type: {
                        str: String,
                        num: Number,
                        bool: Boolean,
                        internal_obj: {
                            int_str: String,
                            int_num: Number,
                            int_bool: Boolean
                        }
                    }
                }
            };
            let body = {
                arr: [
                    {
                        str: 'Elem1',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false
                        }
                    },
                    {
                        str: 'Elem2',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 'This is not a number, should fail',
                            int_bool: false
                        }
                    }
                ]
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when one key in on of the body array elements is not as expected', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    element_type: {
                        str: String,
                        num: Number,
                        bool: Boolean,
                        internal_obj: {
                            int_str: String,
                            int_num: Number,
                            int_bool: Boolean
                        }
                    }
                }
            };
            let body = {
                arr: [
                    {
                        str: 'Elem1',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false
                        }
                    },
                    {
                        str: 'Elem2',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false,
                            an_excessive_key: 'The check should fail, due to this excessive key'
                        }
                    }
                ]
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when one key in on of the body array elements is not as expected', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    element_type: {
                        str: String,
                        num: Number,
                        bool: Boolean,
                        internal_obj: {
                            int_str: String,
                            int_num: Number,
                            int_bool: Boolean
                        }
                    }
                }
            };
            let body = {
                arr: [
                    {
                        str: 'Elem1',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false
                        }
                    },
                    {
                        str: 'Elem2',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false
                        },
                        an_excessive_key: 'The check should fail, due to this excessive key'
                    }
                ]
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when one key in on of the body array elements missing a key', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    element_type: {
                        str: String,
                        num: Number,
                        bool: Boolean,
                        internal_obj: {
                            int_str: String,
                            int_num: Number,
                            int_bool: Boolean
                        }
                    }
                }
            };
            let body = {
                arr: [
                    {
                        str: 'Elem1',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                            int_bool: false // this key is missing, should fail
                        }
                    },
                    {
                        str: 'Elem2',
                        num: 42, 
                        bool: true,
                        internal_obj: {
                            int_str: 'Internal String',
                            int_num: 43,
                        }
                    }
                ]
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    });
});