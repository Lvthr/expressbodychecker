var assert = require('assert');

const bodychecker = require('../bodychecker.js');

function get_mock_req(body) {
    let moch_req = {
        body: body
    };
    return moch_req;
}

function get_mock_res() {
    let moch_res = {
        status: function(statuscode) {},
        json: function(data) {}
    };
    return moch_res;
}

function get_checkdetails(shouldfail, done) {
    if(shouldfail === true) {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(); // the request should fail, call done with no errors
            }
        };
        return checkdetails;
    } else {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(errors); // the request should not fail, call done with errors
            }
        };
        return checkdetails;
    }
}

function check(bodytype, body, shouldfail, done) {
    let moch_req = get_mock_req(body);
    let moch_res = get_mock_res();
    let checkdetails = get_checkdetails(shouldfail, done);   

    if(shouldfail === true) {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done('Error: test succeeded but should have failed'); // Expect the request to succeed
        });
    } else {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done(); // Expect the request to succeed
        });
    }
}

describe('Basic checks test', function() {
    describe('Stringchecker tests', function() {
        it('length parameter succeeds when string has correct length', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    length: 14
                }
            };
            let body = {
                str: 'correct_length'
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        }); 

        it('length parameter fails when string is too short', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    length: 14
                }
            };
            let body = {
                str: 'too_short'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        }); 

        it('length parameter fails when string is too long', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    length: 7
                }
            };
            let body = {
                str: 'too_long'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        }); 

        it('Succeeds when string is longer than minlength', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    minlength: 5
                }
            }

            let body = {
                str: 'longer_than_minlength'
            }
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when string is shorter than minlength', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    minlength: 10
                }
            }

            let body = {
                str: 'too_short'
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Succeeds when string is shorter than maxlength', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    maxlength: 15
                }
            }

            let body = {
                str: 'correct_length'
            }
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when string is longer than maxlength', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    maxlength: 5
                }
            }

            let body = {
                str: 'longer_than_maxlength'
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Succeeds when string is longer than minlength and shorter than maxlength', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    minlength: 5,
                    maxlength: 10
                }
            }

            let body = {
                str: 'correct'
            }
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when string is shorter than minlength (both minlength and maxlength have been provided in bodytype)', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    minlength: 10,
                    maxlength: 20
                }
            }

            let body = {
                str: 'too_short'
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when string is longer than maxlength (both minlength and maxlength have been provided in bodytype)', function(done) {
            let bodytype = {
                str: {
                    type: String,
                    minlength: 1,
                    maxlength: 7
                }
            }

            let body = {
                str: 'too_long'
            }
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    });

    describe('Numberchecker tests', function() {
        it('Succeeds when number is greater than min', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    min: 10
                }
            };
            let body = {
                num: 11
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when number is lower than min', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    min: 10
                }
            };
            let body = {
                num: 9
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Succeeds when number is lower than max', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    max: 10
                }
            };
            let body = {
                num: 9
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when number is greater than max', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    max: 10
                }
            };
            let body = {
                num: 11
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Succeeds when number is greater than min and lower than max', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    min: 5,
                    max: 10
                }
            };
            let body = {
                num: 7.5
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when number is lower than min (both min and max have been provided in bodytype)', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    min: 5,
                    max: 10
                }
            };
            let body = {
                num: 4.99
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when number is greater than max (both min and max have been provided in bodytype)', function(done) {
            let bodytype = {
                num: {
                    type: Number,
                    min: 5,
                    max: 10
                }
            };
            let body = {
                num: 10000
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    });

    describe('Arraychecker tests', function() {
        it('length parameter succeeds when array has correct length', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    length: 2
                }
            };
            let body = {
                arr: ['elem1', 'elem2']
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        }); 

        it('length parameter fails when array is too short', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    length: 3
                }
            };
            let body = {
                arr: ['elem1', 'elem2']
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        }); 

        it('length parameter fails when array is too long', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    length: 1
                }
            };
            let body = {
                arr: ['elem1', 'elem2']
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        }); 

        it('Succeeds when array is longer than minlength', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    minlength: 2
                }
            };
            let body = {
                arr: ['elem1', 'elem2', 'elem3']
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when array is shorter than minlength', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    minlength: 2
                }
            };
            let body = {
                arr: ['elem1']
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Succeeds when array is shorter than maxlength', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    maxlength: 2
                }
            };
            let body = {
                arr: ['elem1']
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when array is longer than maxlength', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    maxlength: 2
                }
            };
            let body = {
                arr: ['elem1', 'elem2', 'elem3']
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Succeeds when array is longer than minlength and shorter than maxlength', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    minlength: 1,
                    maxlength: 3
                }
            };
            let body = {
                arr: ['elem1', 'elem2']
            };
            let shouldfail = false;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when string is shorter than minlength (both minlength and maxlength have been provided in bodytype)', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    minlength: 3,
                    maxlength: 5
                }
            };
            let body = {
                arr: ['elem1', 'elem2']
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when string is longer than maxlength (both minlength and maxlength have been provided in bodytype)', function(done) {
            let bodytype = {
                arr: {
                    type: Array,
                    minlength: 1,
                    maxlength: 4
                }
            };
            let body = {
                arr: ['elem1', 'elem2', 'elem3', 'elem4', 'elem5', 'elem6']
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    });
});