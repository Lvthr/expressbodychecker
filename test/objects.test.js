var assert = require('assert');

const bodychecker = require('../bodychecker.js');

function get_mock_req(body) {
    let moch_req = {
        body: body
    };
    return moch_req;
}

function get_mock_res() {
    let moch_res = {
        status: function(statuscode) {},
        json: function(data) {}
    };
    return moch_res;
}

function get_checkdetails(shouldfail, done) {
    if(shouldfail === true) {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(); // the request should fail, call done with no errors
            }
        };
        return checkdetails;
    } else {
        let checkdetails = {
            checktype: bodychecker.EXPLICIT,
            failurefunction: function(req, res, errors) {
                done(errors); // the request should not fail, call done with errors
            }
        };
        return checkdetails;
    }
}

function check(bodytype, body, shouldfail, done) {
    let moch_req = get_mock_req(body);
    let moch_res = get_mock_res();
    let checkdetails = get_checkdetails(shouldfail, done);   

    if(shouldfail === true) {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done('Error: test succeeded but should have failed'); // Expect the request to succeed
        });
    } else {
        bodychecker.check_body(bodytype, checkdetails)(moch_req, moch_res, (req, res) => {
            done(); // Expect the request to succeed
        });
    }
}

describe('Object tests v1.6.0', function() {
    describe('The body member is an (expected) object, with it\'s own members. The bodytype contains no members with the reserved type keyword, and all members are of basic datatypes (String, Number, Boolean). ', function() {
        it('Succeeds when body is as expected.', function(done) {
            let bodytype = {
                obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                    str: String,
                    num: Number,
                    bool: Boolean
                }
            };
            let body = {
                obj: {
                    str: "String",
                    num: 42,
                    bool: true
                }
            };
            let shouldfail = false;

            check(bodytype, body, shouldfail, done);
        });

        describe('String tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected String is Number', function(done) {
                let bodytype = {
                    obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                        str: String,
                        num: Number,
                        bool: Boolean
                    }
                };
                let body = {
                    obj: {
                        str: 666, // Not a string, expect to fail
                        num: 42, 
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected String is Boolean', function(done) {
                let bodytype = {
                    obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                        str: String,
                        num: Number,
                        bool: Boolean
                    }
                };
                let body = {
                    obj: {
                        str: false, // Not a string, expect to fail
                        num: 42, 
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
        
        describe('Number tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Number is String', function(done) {
                let bodytype = {
                    obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                        str: String,
                        num: Number,
                        bool: Boolean
                    }
                };
                let body = {
                    obj: {
                        str: 'String', 
                        num: '42', // Not a number, expect to fail
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Number is Boolean', function(done) {
                let bodytype = {
                    obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                        str: String,
                        num: Number,
                        bool: Boolean
                    }
                };
                let body = {
                    obj: {
                        str: 'String', 
                        num: false, // Not a number, expect to fail
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });

        describe('Boolean tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Boolean is Number', function(done) {
                let bodytype = {
                    obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                        str: String,
                        num: Number,
                        bool: Boolean
                    }
                };
                let body = {
                    obj: {
                        str: 'String', 
                        num: 42, 
                        bool: 666 // Not a boolean, expect to fail
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Boolean is String', function(done) {
                let bodytype = {
                    obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                        str: String,
                        num: Number,
                        bool: Boolean
                    }
                };
                let body = {
                    obj: {
                        str: 'String', 
                        num: false, 
                        bool: 'should fail' // Not a boolean, expect to fail
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
    });

    describe('The body member is an (expected) object, with it\'s own members. The bodytype DOES contain members with the reserved type keyword, and all members are of basic datatypes (String, Number, Boolean, not Array)', function() {
        const bodytype = {
            obj: { // The body should contain a key with name "obj" that is an object with it's own keys (str, num, bool)
                str: {
                    type: String
                },
                num: {
                    type: Number
                },
                bool: {
                    type: Boolean
                }
            }
        };
        it('Succeeds when the body is as expected', function(done) {
            let body = {
                obj: {
                    str: "String",
                    num: 42,
                    bool: true
                }
            };
            let shouldfail = false;

            check(bodytype, body, shouldfail, done);
        });

        describe('String tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected String is Number', function(done) {
                let body = {
                    obj: {
                        str: 666, // Not a string, expect to fail
                        num: 42, 
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected String is Boolean', function(done) {
                let body = {
                    obj: {
                        str: false, // Not a string, expect to fail
                        num: 42, 
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
        
        describe('Number tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Number is String', function(done) {
                let body = {
                    obj: {
                        str: 'String', 
                        num: '42', // Not a number, expect to fail
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Number is Boolean', function(done) {
                let body = {
                    obj: {
                        str: 'String', 
                        num: false, // Not a number, expect to fail
                        bool: true
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });

        describe('Boolean tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Boolean is Number', function(done) {
                let body = {
                    obj: {
                        str: 'String', 
                        num: 42, 
                        bool: 666 // Not a boolean, expect to fail
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Boolean is String', function(done) {
                let body = {
                    obj: {
                        str: 'String', 
                        num: false, 
                        bool: 'should fail' // Not a boolean, expect to fail
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
    });

    describe('The body member is an expected object, with it\'s own members, where the member itself contains other objects. None of the body members uses the type keyword.', function() {
        const bodytype = {
            outer_object: {
                inner_object: {
                    str: String,
                    num: Number,
                    bool: Boolean
                }
            }
        };
        it('Should succeed when the body is as expected', function(done) {
            let body = {
                outer_object: {
                    inner_object: {
                        str: "String",
                        num: 42,
                        bool: true
                    }
                } 
            };
            let shouldfail = false;

            check(bodytype, body, shouldfail, done);
        });
        
        describe('String tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected String is Number', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 666, // Not a string, expect to fail
                            num: 42, 
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected String is Boolean', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: false, // Not a string, expect to fail
                            num: 42, 
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
        
        describe('Number tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Number is String', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: '42', // Not a number, expect to fail
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Number is Boolean', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: false, // Not a number, expect to fail
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });

        describe('Boolean tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Boolean is Number', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: 42, 
                            bool: 666 // Not a boolean, expect to fail
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Boolean is String', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: false, 
                            bool: 'should fail' // Not a boolean, expect to fail
                        }
                    }
                };  
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
    });

    describe('The body member is an expected object, with it\'s own members, where the member itself contains other objects. All of the body members uses the type keyword.', function() {
        const bodytype = {
            outer_object: {
                inner_object: {
                    str: {
                        type: String
                    },
                    num: {
                        type: Number
                    },
                    bool: {
                        type: Boolean
                    }
                }
            }
        };
        it('Should succeed when the body is as expected', function(done) {
            let body = {
                outer_object: {
                    inner_object: {
                        str: "String",
                        num: 42,
                        bool: true
                    }
                } 
            };
            let shouldfail = false;

            check(bodytype, body, shouldfail, done);
        });
        
        describe('String tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected String is Number', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 666, // Not a string, expect to fail
                            num: 42, 
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected String is Boolean', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: false, // Not a string, expect to fail
                            num: 42, 
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
        
        describe('Number tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Number is String', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: '42', // Not a number, expect to fail
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Number is Boolean', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: false, // Not a number, expect to fail
                            bool: true
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });

        describe('Boolean tests. All should fail.', function() {
            it('Fails when body is NOT as expected. Expected Boolean is Number', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: 42, 
                            bool: 666 // Not a boolean, expect to fail
                        }
                    }
                };
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
    
            it('Fails when body is NOT as expected. Expected Boolean is String', function(done) {
                let body = {
                    outer_object: {
                        inner_object: {
                            str: 'String', 
                            num: false, 
                            bool: 'should fail' // Not a boolean, expect to fail
                        }
                    }
                };  
                let shouldfail = true;
    
                check(bodytype, body, shouldfail, done);
            });
        });
    });

    describe('The body contains several members that are objects, with their own internal object members.', function() {
        const bodytype = {
            outer_object_1: {
                inner_object_1_1: {
                    str: String
                },
                inner_object_1_2: {
                    num: Number
                }
            },
            outer_object_2: {
                inner_object_2_1: {
                    bool: Boolean
                },
                inner_object_2_2: {
                    str: String
                }
            }
        };
        it('Succeeds when the body is as expected', function(done) {
            let body = {
                outer_object_1: {
                    inner_object_1_1: {
                        str: 'String'
                    },
                    inner_object_1_2: {
                        num: 42
                    }
                },
                outer_object_2: {
                    inner_object_2_1: {
                        bool: true
                    },
                    inner_object_2_2: {
                        str: 'String'
                    }
                }
            };
            let shouldfail = false;
    
            check(bodytype, body, shouldfail, done);
        });

        describe('Fails when one of the internal member\'s keys are not as expected', function() {
            it('Should fail', function(done) {
                let body = {
                    outer_object_1: {
                        inner_object_1_1: {
                            str: 'String'
                        },
                        inner_object_1_2: {
                            num: '42' // Not a number, should fail
                        }
                    },
                    outer_object_2: {
                        inner_object_2_1: {
                            bool: true
                        },
                        inner_object_2_2: {
                            str: 'String'
                        }
                    }
                };
                let shouldfail = true;
        
                check(bodytype, body, shouldfail, done);
            });
        });
    });

    describe('Fails when a key is missing', function() {
        it('Fails when the missing body member should have been a String', function(done) {
            let bodytype = {
                str: String,
                num: Number
            };
            let body = {
                num: 1
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the missing body member should have been a Number', function(done) {
            let bodytype = {
                str: String,
                num: Number
            };
            let body = {
                str: 'The string is present but the number is missing'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the missing body member should have been a Boolean', function(done) {
            let bodytype = {
                str: String,
                bool: Number
            };
            let body = {
                str: 'The string is present but the boolean is missing'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the missing body member should have been a Object', function(done) {
            let bodytype = {
                str: String,
                obj: {
                    str1: String,
                    num2: Number
                }
            };
            let body = {
                str: 'The string is present, but the object is completley missing'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the missing body member is an Object, but one of it\'s internal members are missing', function(done) {
            let bodytype = {
                str: String,
                obj: {
                    str1: String,
                    num2: Number
                }
            };
            let body = {
                str: 'The string is present, but the object is missing the number key',
                obj: {
                    str1: 'The internal string is present, but the obj is missing the number'
                }
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });

        it('Fails when the missing body member should have been a Array', function(done) {
            let bodytype = {
                str: String,
                arr: {
                    type: Array
                }
            };
            let body = {
                str: 'The string is present, but the array is completley missing'
            };
            let shouldfail = true;
            check(bodytype, body, shouldfail, done);
        });
    }); 

    describe('Checktypes tests.', function() {
        describe('Key is present in the body that is not (explicitly) described by the bodytype description', function() {
            it('Fails when the excessive key is present in the first layer of the body (no recursion)', function(done) {
                let bodytype = {
                    str: String,
                    num: Number,
                    bool: Boolean
                };
                let body = {
                    str: 'There is an excessive key in the body',
                    num: 666,
                    bool: false,
                    excessive_key: 'This is the excessive key, should fail'
                };
                let shouldfail = true;
                check(bodytype, body, shouldfail, done);
            });
            
            it('Fails when the excessive key is present inside an internal (expected) object in the body (recursion should occur)', function(done) {
                let bodytype = {
                    str: String,
                    num: Number,
                    bool: Boolean,
                    obj: {
                        str: String
                    }
                };
                let body = {
                    str: 'There is an excessive key in the body',
                    num: 666,
                    bool: false,
                    obj: {
                        str: 'This key is expected, but the key below is not',
                        excessive_key: 'This is the excessive key, should fail'
                    }
                };
                let shouldfail = true;
                check(bodytype, body, shouldfail, done);
            });
    

        });
        
    });
});