function random_string(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const RANDOM_KEY_LENGTH = 10;
const TYPES = [String, Number, Boolean, Object, Array];

function randomtype() {
    return TYPES[Math.floor(Math.random() * TYPES.length)];
}

function typestring(typedef) {
    if(typedef.toString() === 'function String() { [native code] }') {
        return 'String'
    } else if(typedef.toString() === 'function Number() { [native code] }') {
        return 'Number'
    } else if(typedef.toString() === 'function Boolean() { [native code] }') {
        return 'Boolean'
    } else if(typedef.toString() === 'function Array() { [native code] }') {
        return 'Array'
    } else if(typedef.toString() === 'function Object() { [native code] }') {
        return 'Object'
    } else {
        return null
    }
}

function typestring_to_type(type_string) {
    if(type_string === 'String') {
        let string = random_string(100);
        return string;
    } else if(type_string === 'Number') {
        let random_number = Math.floor(Math.random() * 100) + 1;
        return random_number;
    } else if(type_string === 'Boolean') {
        let random_int = Math.floor(Math.random() * 100) + 1;
        if(random_int % 2 === 0) {
            return true;
        } else {
            return false;
        }
    } else if(type_string === 'Object') {

    } else if(type_string === 'Array') {

    } else {
        throw Error('Could not identify type_string')
    }
}

function generate_random_bodytype_and_body(number_of_members, max_depth, parent_type, parent_body, parent_key) {
    let bodytype = {};
    let body = {};

    for(let i = 0; i < number_of_members; i++) {
        let key = random_string(RANDOM_KEY_LENGTH);
        let type = randomtype();
        let tps = typestring(type);

        if(tps === 'Object') {
            if(max_depth > 0) {
                let ret = generate_random_bodytype_and_body(number_of_members, max_depth - 1, bodytype, body, key);
                bodytype[key] = ret.bodytype;
                body[key] = ret.body;
            }
        } else if(tps === 'Array') {
            let number_of_elements = Math.floor(Math.random() * 100) + 1;
            let type_of_elements = randomtype();
            let element_typestring = typestring(type_of_elements);

            let array = [];
            if(element_typestring === 'String') {
                for(let j = 0; j < number_of_elements; j++) {
                    array.push(random_string(20));
                }
            } else if(element_typestring === 'Number') {
                for(let j = 0; j < number_of_elements; j++) {
                    array.push(Math.floor(Math.random(100) + 1));
                }
            } else if(element_typestring === 'Boolean') {
                for(let j = 0; j < number_of_elements; j++) {
                    let random_int = Math.floor(Math.random() * 100) + 1;
                    let value;
                    if(random_int % 2 === 0) {
                        value = true;
                    } else {
                        value = false;
                    }
                    array.push(value);
                }
            }
        } else {
            bodytype[key] = type;
            body[key] = typestring_to_type(tps);
        }
    }

    return {bodytype: bodytype, body: body}
}

function random_bodytype_and_body() {
    let bd = {};
    let bdt = {};
    let output = generate_random_bodytype_and_body(10, 10, bdt, bd, null)
    return output;
}

module.exports = random_bodytype_and_body;