const MISSING_KEY_CODE = 1;
const MISSING_KEY = (key, typedef) => {
    let output = {
        code: MISSING_KEY_CODE,
        error: 'Missing body key ' + key + ' with expeted type of ' + typedef,
        key: key,
        expected_type: typedef
    };
    return output;
};

const INCORRECT_TYPE_CODE = 2;
const INCORRECT_TYPE = (key, typedef, found_type) => {
    let output = {
        code: INCORRECT_TYPE_CODE,
        error: 'Error: key ' + key + ' is not of correct type ' + typedef + '. Got type ' + found_type,
        key: key,
        expected_type: typedef,
        found_type: found_type
    };
    return output;
};

const EXCESSIVE_KEY_CODE = 3;
const EXCESSIVE_KEY = (key, found_type) => {
    let output = {
        code: EXCESSIVE_KEY_CODE,
        error: 'Error: body contains unexpected key ' + key + ' of type ' + found_type + ' that is not allowed',
        key: key,
        found_type: found_type
    };
    return output;
};

const ARRAY_ELEMENT_MISSING_KEY_CODE = 4;
const ARRAY_ELEMENT_MISSING_KEY = (index, key, typedef) => {
    let output = {
        code: ARRAY_ELEMENT_MISSING_KEY_CODE,
        error: 'Error: Array element ' + index + ' is missing key ' + key + ' of expected type ' + typedef,
        index: index,
        key: key,
        expected_type: typedef
    }
    return output;
};

const ARRAY_ELEMENT_INCORRECT_TYPE_CODE = 5;
const ARRAY_ELEMENT_INCORRECT_TYPE = (index, key, typedef, found_type) => {
    let output = {
        code: ARRAY_ELEMENT_INCORRECT_TYPE_CODE,
        error: 'Error: Array element ' + index + ' is not of correct type ' + typedef + '. Got type ' + found_type,
        index: index,
        key: key,
        expected_type: typedef,
        found_type: found_type
    };
    return output;
};

const ARRAY_ELEMENT_EXCESSIVE_KEY_CODE = 6;
const ARRAY_ELEMENT_EXCESSIVE_KEY = (index, key, found_type) => {
    let output = {
        code: ARRAY_ELEMENT_EXCESSIVE_KEY_CODE,
        error: 'Error: Array element ' + index + ' contains unexpected key ' + key + ' with type ' + found_type + ' that is not allowed',
        index: index,
        key: key,
        found_type: found_type
    };
    return output;
}

const PARAMETER_ERROR_STRING_CODE = 7;
const PARAMETER_ERROR_STRING = (message, value, keyname, minlength, maxlength, length) => {
    let output = {
        code: PARAMETER_ERROR_STRING_CODE,
        error: 'Error: ' + message,
        value: value,
        key: keyname,
        found_length: value.length,
        parameters: {
            minlength: minlength,
            maxlength: maxlength,
            length: length
        }
    };
    return output;
};
const PARAMETER_ERROR_NUMBER_CODE = 8;
const PARAMETER_ERROR_NUMBER = (message, value, keyname, min, max) => {
    let output = {
        code: PARAMETER_ERROR_NUMBER_CODE,
        error: 'Error: ' + message,
        value: value,
        key: keyname,
        parameters: {
            min: min,
            max: max
        }
    };
    return output;
};

const PARAMETER_ERROR_ARRAY_CODE = 9;
const PARAMETER_ERROR_ARRAY = (message, array, keyname, minlength, maxlength, length) => {
    let output = {
        code: PARAMETER_ERROR_ARRAY_CODE,
        error: 'Error: ' + message,
        value: array,
        key: keyname,
        found_length: array.length,
        parameters: {
            minlength: minlength,
            maxlength: maxlength,
            length: length
        }
    };
    return output;
};

module.exports = {
    functions: {
        MISSING_KEY: MISSING_KEY,
        INCORRECT_TYPE: INCORRECT_TYPE,
        EXCESSIVE_KEY: EXCESSIVE_KEY,
        ARRAY_ELEMENT_MISSING_KEY: ARRAY_ELEMENT_MISSING_KEY,
        ARRAY_ELEMENT_INCORRECT_TYPE: ARRAY_ELEMENT_INCORRECT_TYPE,
        ARRAY_ELEMENT_EXCESSIVE_KEY: ARRAY_ELEMENT_EXCESSIVE_KEY,
        PARAMETER_ERROR_STRING: PARAMETER_ERROR_STRING,
        PARAMETER_ERROR_NUMBER: PARAMETER_ERROR_NUMBER,
        PARAMETER_ERROR_ARRAY: PARAMETER_ERROR_ARRAY
    },
    codes: {
        MISSING_KEY_CODE: MISSING_KEY_CODE,
        INCORRECT_TYPE_CODE: INCORRECT_TYPE_CODE,
        EXCESSIVE_KEY_CODE: EXCESSIVE_KEY_CODE,
        ARRAY_ELEMENT_MISSING_KEY_CODE: ARRAY_ELEMENT_MISSING_KEY_CODE,
        ARRAY_ELEMENT_INCORRECT_TYPE_CODE: ARRAY_ELEMENT_INCORRECT_TYPE_CODE,
        ARRAY_ELEMENT_EXCESSIVE_KEY_CODE: ARRAY_ELEMENT_EXCESSIVE_KEY_CODE,
        PARAMETER_ERROR_STRING_CODE: PARAMETER_ERROR_STRING_CODE,
        PARAMETER_ERROR_NUMBER_CODE: PARAMETER_ERROR_NUMBER_CODE,
        PARAMETER_ERROR_ARRAY_CODE: PARAMETER_ERROR_ARRAY_CODE
    }
};