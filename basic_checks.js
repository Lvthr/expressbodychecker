const ERRORS = require('./errors.js');

function stringchecker(value, keyname, parameters, callback) {
    // minlength, maxlength, length
    if(parameters.length !== undefined) {
        if(value.length === parameters.length) {
            callback(true, null);
        } else {
            let error = ERRORS.functions.PARAMETER_ERROR_STRING(
                'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required length. Must be equal to ' + parameters.length,
                value, keyname, parameters.minlength, parameters.maxlength, parameters.length
            );
            callback(false, error);
        }
    } else {
        if(parameters.minlength !== undefined && parameters.maxlength !== undefined) {
            if(value.length >= parameters.minlength && value.length <= parameters.maxlength) {
                callback(true, null);
            } else {
                let error = ERRORS.functions.PARAMETER_ERROR_STRING(
                    'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required length. Must be greather (or equal to) ' + parameters.minlength + ' and lower than (or equal to) ' + parameters.maxlength,
                    value, keyname, parameters.minlength, parameters.maxlength, parameters.length
                );
                callback(false, error);
            }
        } else if(parameters.minlength !== undefined && parameters.maxlength === undefined) {
            if(value.length >= parameters.minlength) {
                callback(true, null);
            } else {
                let error = ERRORS.functions.PARAMETER_ERROR_STRING(
                    'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required length. Must be greather (or equal to) ' + parameters.minlength,
                    value, keyname, parameters.minlength, parameters.maxlength, parameters.length
                );
                callback(false, error);
            }
        } else if(parameters.minlength === undefined && parameters.maxlength !== undefined){ // minlength === undefined && maxlength !== undefined
            if(value.length <= parameters.maxlength) {
                callback(true, null);
            } else {
                let error = ERRORS.functions.PARAMETER_ERROR_STRING(
                    'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required length. Must be lower than (or equal to) ' + parameters.maxlength,
                    value, keyname, parameters.minlength, parameters.maxlength, parameters.length
                );
                callback(false, error);
            }
        } else {
            callback(true, null);
        }
    }
}

function numberchecker(value, keyname, parameters, callback) {
    if(parameters.min !== undefined && parameters.max !== undefined) {
        if(value >= parameters.min && value <= parameters.max) {
            callback(true, null);
        } else {
            let error = ERRORS.functions.PARAMETER_ERROR_NUMBER(
                'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required parameters. Must be greather (or equal to) ' + parameters.min + ' and lower than (or equal to) ' + parameters.max,
                value, keyname, parameters.min, parameters.max
            );
            callback(false, error);            
        }
    } else if(parameters.min !== undefined && parameters.max === undefined) {
        if(value >= parameters.min) {
            callback(true, null);
        } else {
            let error = ERRORS.functions.PARAMETER_ERROR_NUMBER(
                'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required parameters. Must be greather (or equal to) ' + parameters.min,
                value, keyname, parameters.min, parameters.max
            );
            callback(false, error);
        }
    } else if(parameters.min === undefined && parameters.max !== undefined) { // min === undefind && max !== undefined
        if(value <= parameters.max) {
            callback(true, null);
        } else {
            let error = ERRORS.functions.PARAMETER_ERROR_NUMBER(
                'key \"' + keyname + '\" with value \"' +  value + '\" does not meat required parameters. Must be lower than (or equal to) ' + parameters.max,
                value, keyname, parameters.min, parameters.max
            );
            callback(false, error);            
        }
    } else {
        callback(true, null);
    }
}

function arraychecker(array, keyname, parameters, callback) {
    if(parameters.length !== undefined) {
        if(array.length === parameters.length) {
            console.log('correct length')
            callback(true, null);
        } else {
            let error = ERRORS.functions.PARAMETER_ERROR_ARRAY(
                'key \"' + keyname + '\" does not meat required length. Must be exactly ' + parameters.length,
                array, keyname, parameters.minlength, parameters.maxlength, parameters.length
            );
            callback(false, error);
        }
    } else {
        if(parameters.minlength !== undefined && parameters.maxlength !== undefined) {
            if(array.length >= parameters.minlength && array.length <= parameters.maxlength) {
                callback(true, null);
            } else {
                let error = ERRORS.functions.PARAMETER_ERROR_ARRAY(
                    'key \"' + keyname + '\" does not meat required length. Must be greather (or equal to) ' + parameters.minlength + ' and lower than (or equal to) ' + parameters.maxlength,
                    array, keyname, parameters.minlength, parameters.maxlength, parameters.length
                );
                callback(false, error);
            }
        } else if(parameters.minlength !== undefined && parameters.maxlength === undefined) {
            if(array.length >= parameters.minlength) {
                callback(true, null);
            } else {
                let error = ERRORS.functions.PARAMETER_ERROR_ARRAY(
                    'key \"' + keyname + '\" does not meat required length. Must be greather (or equal to) ' + parameters.minlength,
                    array, keyname, parameters.minlength, parameters.maxlength, parameters.length
                );
                callback(false, error);
            }
        } else if(parameters.minlength === undefined && parameters.maxlength !== undefined) { // minlength === undefined && maxlength !== undefined
            if(array.length <= parameters.maxlength) {
                callback(true, null);
            } else {
                let error = ERRORS.functions.PARAMETER_ERROR_ARRAY(
                    'key \"' + keyname + '\" does not meat required length. Must be lower than (or equal to) ' + parameters.maxlength,
                    array, keyname, parameters.minlength, parameters.maxlength, parameters.length
                );
                callback(false, error);
            }
        } else {
            // default no error
            callback(true, null);
        }
    }
}

module.exports = {
    stringchecker: stringchecker,
    numberchecker: numberchecker,
    arraychecker: arraychecker
}