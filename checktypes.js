const EXPLICIT = 1;
const REQUIRED = 2;

const checktypes = {
    REQUIRED: REQUIRED,
    EXPLICIT: EXPLICIT,
    DEFAULT: EXPLICIT
};

module.exports = checktypes;